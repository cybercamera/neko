extends Node2D

export var speed_max :int = 9.5
export var appearance_delay_min : int  = 2
export var appearance_delay_max : int  = 5


enum GAME_STATE {MOVE_NORMAL, MOVE_SPRITE_LEFT, MOVE_SPRITE_LEFT_REVERSE_RIGHT, MOVE_SPRITE_RIGHT, MOVE_SPRITE_RIGHT_REVERSE_LEFT, OFF_SCREEN, PAUSED}
enum MOVE_DIRECTION {LEFT, RIGHT}
var game_state: int = GAME_STATE.MOVE_NORMAL
var game_state_previous: int = GAME_STATE.MOVE_NORMAL
var move_direction: int = MOVE_DIRECTION.RIGHT
var move_sprite_direction: int = MOVE_DIRECTION.RIGHT

var popup_menu_items = [
	"Pat Neko",
	"Feed Neko",
	"Increase Volume",
	"Decrease Volume",
	"Increase Appearances",
	"Decrease Appearances",
	"Rainbow Cat",
	"About Neko",
	"Quit"
	]

var vocalisation_stream_array = [
	"res://assets/sounds/meowing1.wav", 
	"res://assets/sounds/meowing2.wav", 
	"res://assets/sounds/meowing3.wav",
	"res://assets/sounds/meowing4.wav", 
	"res://assets/sounds/meowing5.wav", 
	"res://assets/sounds/meowing6.wav",
	"res://assets/sounds/meowing7.wav", 
	"res://assets/sounds/meowing8.wav", 
	"res://assets/sounds/meowing9.wav",
	"res://assets/sounds/meowing10.wav", 
	"res://assets/sounds/meowing11.wav", 
	"res://assets/sounds/meowing12.wav",
	"res://assets/sounds/meowing13.wav", 
	"res://assets/sounds/purring1.wav", 
	"res://assets/sounds/purring2.wav", 
	"res://assets/sounds/purring3.wav", 
	"res://assets/sounds/purring4.wav", 
	"res://assets/sounds/soft_meowing1.wav",
	"res://assets/sounds/soft_meowing2.wav",
	"res://assets/sounds/soft_meowing3.wav",
	"res://assets/sounds/soft_meowing4.wav"
	]

var purring_stream_array = [
	
	"res://assets/sounds/purring1.wav", 
	"res://assets/sounds/purring2.wav", 
	"res://assets/sounds/purring3.wav", 
	"res://assets/sounds/purring4.wav"
	]

var meowing_stream_array = [
	"res://assets/sounds/meowing1.wav", 
	"res://assets/sounds/meowing2.wav", 
	"res://assets/sounds/meowing3.wav",
	"res://assets/sounds/meowing4.wav", 
	"res://assets/sounds/meowing5.wav", 
	"res://assets/sounds/meowing6.wav",
	"res://assets/sounds/meowing7.wav", 
	"res://assets/sounds/meowing8.wav", 
	"res://assets/sounds/meowing9.wav",
	"res://assets/sounds/meowing10.wav", 
	"res://assets/sounds/meowing11.wav", 
	"res://assets/sounds/meowing12.wav",
	"res://assets/sounds/meowing13.wav", 
	] 

var soft_meowing_stream_array = [
	"res://assets/sounds/soft_meowing1.wav",
	"res://assets/sounds/soft_meowing2.wav",
	"res://assets/sounds/soft_meowing3.wav",
	"res://assets/sounds/soft_meowing4.wav"
	]

var available_animations = [
	"jumping",
	"sliding",
	"walking",
	"transition_walking_to_running",
	"running"
	]

var sound_queue = []
var animation_queue = []

var vel : Vector2 = Vector2.ZERO #this holds the window movement
var sprite_vel : Vector2 = Vector2.ZERO #This holds the in-window sprite movement
var sprite_pos_zero : Vector2 = Vector2(0, 30)
var speed_multi : int = speed_max
var speed_slow : int = speed_max/2
var global_mouse_pos #= OS.get_window_position() + get_global_mouse_position()
var screen_size : Vector2
var window_size : Vector2
var neko_paused : bool = false
var popup_menu_active : bool = false
var mouse_hover_over_cat : bool = false
var current_volume_level : float
var move_sprite_left : bool = false
var move_sprite_right : bool = false
var move_sprite_completed : bool = false
var move_sprite_left_reverse_right : bool = false
var move_sprite_right_reverse_left : bool = false
var moved_off_left_screen : bool = false
var moved_off_right_screen : bool = false
var sprite_reverse_move : bool = false
var is_cat_rainbow : bool = false
var check_limbo_pos : Vector2
var real_screen_left_edge : int = 0 # Used to establish a real screen left edge, ignoring panels
var real_screen_right_edge : int = 0 # Used to establish a real screen right edge, ignoring panels

var current_animation : String

func _ready() -> void:
	randomize()
	get_tree().get_root().set_transparent_background(true)
	OS.window_per_pixel_transparency_enabled = true
	screen_size = OS.get_screen_size()
	print("screen_size: ", str(screen_size))
	window_size = OS.window_size
	real_screen_left_edge = 0 # Left-hand edge of screen
	real_screen_right_edge = screen_size.x # Assume full screen width for now64 pixels for any right panel
	add_animation_to_queue()
	current_animation = animation_queue[0]
	$AnimatedSprite.play(current_animation)
	new_velocity(MOVE_DIRECTION.RIGHT)
	game_state = GAME_STATE.MOVE_NORMAL 
	 #Set both the velocity of the window and the (when started) velocity of the sprite to be the same
	#Establish the master volume levels
	current_volume_level = AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Master"))
	#Set the starting position to be middle of tthe screen
	OS.window_position.x = (screen_size.x - window_size.x)/2
	OS.window_position.y = (screen_size.y - window_size.y)/2
	$OffscreenDelayTimer.wait_time = rand_range(appearance_delay_min, appearance_delay_max)
	for i in popup_menu_items:
		$PopupMenu.add_item(i)

func _process(delta: float) -> void:
	match game_state:
		GAME_STATE.MOVE_NORMAL:
			OS.window_position += vel*speed_multi
			if OS.window_position.x <=  real_screen_left_edge and game_state_previous != GAME_STATE.MOVE_SPRITE_LEFT_REVERSE_RIGHT:
				#We've reached the left edge of the screen. 
				#if we're not already doing so. we need to now transition to moving the sprite within the window, rather than window within the screen
				sprite_vel = vel #Set the sprite vel to be the same as the window's
				change_game_state(GAME_STATE.MOVE_NORMAL, GAME_STATE.MOVE_SPRITE_LEFT)
				
			elif OS.window_position.x >=  real_screen_right_edge and game_state_previous != GAME_STATE.MOVE_SPRITE_RIGHT_REVERSE_LEFT:
				#We've reached the right edge of the screen. 
				#if we're not already doing so. we need to now transition to moving the sprite within the window, rather than window within the screen
				sprite_vel = vel #Set the sprite vel to be the same as the window's
				change_game_state(GAME_STATE.MOVE_NORMAL, GAME_STATE.MOVE_SPRITE_RIGHT)
			
			
		GAME_STATE.MOVE_SPRITE_LEFT:
			#We are here because we want to move the sprite within the window rather than window within the screen
			#print("$AnimatedSprite.position.x: ", str($AnimatedSprite.position.x), " sprite_vel:  ", str(sprite_vel))
			if $AnimatedSprite.position.x > - window_size.x:
				#print("$AnimatedSprite.position.x: ", str($AnimatedSprite.position.x), " window_width:  ", str(window_size))
				#We haven't finished moving the sprite to the left fully yet
				$AnimatedSprite.position.x += (sprite_vel*speed_multi).x
			else:
				#We have finished moving the sprite to the left, thus switch off sprite moving
				#Now we trigger a delay
				off_screen_period()
				#And change machine state
				change_game_state(GAME_STATE.MOVE_SPRITE_LEFT, GAME_STATE.OFF_SCREEN)
				#Finally we set a new left_reverse_right vel vector to get us moving right
				new_sprite_velocity(MOVE_DIRECTION.LEFT)
				
			
		GAME_STATE.MOVE_SPRITE_LEFT_REVERSE_RIGHT:
			#We are here because we want to reverse the sprite back within the window rather than window within the screen
			#print("$AnimatedSprite.position.x: ", str($AnimatedSprite.position.x), " sprite_vel:  ", str(sprite_vel))
			if $AnimatedSprite.position.x <= 0:
				#We haven't finished moving the sprite to the left fully yet
				$AnimatedSprite.position.x += (sprite_vel*speed_multi).x
			else:	
				#We are now back on-screen, heading RIGHT. Next state is MOVE_NORMAL
				change_game_state(GAME_STATE.MOVE_SPRITE_LEFT_REVERSE_RIGHT, GAME_STATE.MOVE_NORMAL)
				#And we set a velocity vector
				new_velocity(MOVE_DIRECTION.RIGHT)
				$AnimatedSprite.position = sprite_pos_zero
				
				
		GAME_STATE.MOVE_SPRITE_RIGHT:
			#We are here because we want to move the sprite within the window rather than window within the screen
			if $AnimatedSprite.position.x < window_size.x:
				#print("$AnimatedSprite.position.x: ", str($AnimatedSprite.position.x), " sprite_vel:  ", str(sprite_vel))
				#We haven't finished moving the sprite to the left fully yet
				$AnimatedSprite.position.x += (sprite_vel*speed_multi).x
			else:
				#We have finished moving the sprite to the right, thus switch off sprite moving
				#Now we trigger a delay
				off_screen_period()
				#And change machine state
				change_game_state(GAME_STATE.MOVE_SPRITE_RIGHT, GAME_STATE.OFF_SCREEN)
				#Finally we set a new left_reverse_right vel vector to get us moving right
				new_sprite_velocity(MOVE_DIRECTION.LEFT)
		
		
		GAME_STATE.MOVE_SPRITE_RIGHT_REVERSE_LEFT:
			#We are here because we want to move the sprite within the window rather than window within the screen
			if $AnimatedSprite.position.x > 0:
				#print("$AnimatedSprite.position.x: ", str($AnimatedSprite.position.x), " sprite_vel:  ", str(sprite_vel))
				#We haven't finished moving the sprite to the left fully yet
				$AnimatedSprite.position.x += (sprite_vel*speed_multi).x
			else:
				#We have finished moving the sprite to the left, thus switch off sprite moving
				#Now we trigger a delay
				new_velocity(MOVE_DIRECTION.LEFT)
				#And change machine state
				change_game_state(GAME_STATE.MOVE_SPRITE_RIGHT_REVERSE_LEFT, GAME_STATE.MOVE_NORMAL)
				$AnimatedSprite.position = sprite_pos_zero
			
		
		GAME_STATE.OFF_SCREEN:
			pass
		
		
		GAME_STATE.PAUSED:
			pass
		


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		quit()

func about_neko() -> void:
	$AboutDialog.show()

func add_animation_to_queue() -> void:
	if animation_queue.size() < 10:
		#This routine adds an animation to the animation queue, if that queue is less than 10 items long.		
		animation_queue.append("walking")
		animation_queue.append("transition_walking_to_running")
		animation_queue.append("running")
		animation_queue.append("jumping")
		animation_queue.append("sliding")
		animation_queue.append("walking")
		
#	return
#	if animation_queue.size() < 10:
#		#Add an item to the queue
#		#If the last item in the queue is a jump or a slide, we need to do a transition to walking.
#		#if the last item is a walk, add at least one more walk in, before we do a jump or slide
#		var animation_to_play = available_animations[randi() % available_animations.size()]
#		animation_queue.append(animation_to_play)
		

func change_game_state(curr_state, new_state) -> void:
	#Helper function to change state while retaining the former state
	#print("change game state: was ", str(1), " now ", str(new_state))
	game_state_previous = curr_state
	game_state = new_state

func cat_meowing() -> void:
	var clip_to_play = meowing_stream_array[randi() % meowing_stream_array.size()]
	sound_queue.insert(0, clip_to_play)
	if !$CatVocalisations.is_playing():
		#If we're not currently playing something from the queue, play this
		play_next_vocalisation()

func cat_purring() -> void:
	var clip_to_play = purring_stream_array[randi() % purring_stream_array.size()]
	sound_queue.insert(0, clip_to_play)
	if !$CatVocalisations.is_playing():
		#If we're not currently playing something from the queue, play this
		play_next_vocalisation()

func cat_soft_meowing() -> void:
	var clip_to_play = soft_meowing_stream_array[randi() % soft_meowing_stream_array.size()]
	sound_queue.insert(0, clip_to_play)
	if !$CatVocalisations.is_playing():
		#If we're not currently playing something from the queue, play this
		play_next_vocalisation()

func cat_vocalisation() -> void:
	var clip_to_play = vocalisation_stream_array[randi() % vocalisation_stream_array.size()]
	sound_queue.insert(0, clip_to_play)
	if !$CatVocalisations.is_playing():
		#If we're not currently playing something from the queue, play this
		play_next_vocalisation()




func check_limbo_status() -> void:
	# Check for limbo
	# Let's check to see if we've hit a panel on the left or right of the
	# screen which would require us to reset the edges of the available screen
	if game_state == GAME_STATE.MOVE_NORMAL:
		#We should only be in limbo if we're in normal movement
		if check_limbo_pos.x == OS.window_position.x:
			# If the window is the same as it was perviously, we're in limbo
			# Establish the new left
			if move_direction == MOVE_DIRECTION.LEFT:
				# reset the left edge to here.
				real_screen_left_edge = OS.window_position.x
				print_diagnostics()
			else:
				# reset the right edge to here, subtracting from the full screen width
				real_screen_right_edge =  OS.window_position.x
				print_diagnostics()
				
		else:
			#We're not yet in a limbo state. Assign the check limbo pos to here
			check_limbo_pos.x = OS.window_position.x
			print_diagnostics()

func increase_volume() -> void:
	current_volume_level += 1
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), current_volume_level)

func decrease_volume() -> void:
	current_volume_level -= 1
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), current_volume_level)

func decrease_appearances() -> void:
	appearance_delay_min += 10
	print("appearance_delay_min ", str(appearance_delay_min))
	
func increase_appearances() -> void:
	appearance_delay_min -= 10
	appearance_delay_min = clamp(appearance_delay_min, 10, 20)
	print("appearance_delay_min ", str(appearance_delay_min))
	

func feed_neko() -> void:
	for i in rand_range(1,2):
		cat_soft_meowing()
		cat_meowing()
	
func pat_neko() -> void:
	cat_purring()
	cat_soft_meowing()

func new_velocity(direction) -> void:
	
	match direction:
		MOVE_DIRECTION.LEFT:
			vel = Vector2(rand_range(-1.0,-0.5), rand_range(-0.2,0.2)).normalized()
			move_direction = direction
		MOVE_DIRECTION.RIGHT:
			vel = Vector2(rand_range(0.5,1.0), rand_range(-0.2,0.2)).normalized()
			move_direction = direction
			
	#We've changed velocity. Let's see if we need to flip the sprite to face the right way
	if vel.x < 0:
		$AnimatedSprite.flip_h = true
		
	elif vel.x > 0 :
		$AnimatedSprite.flip_h = false
		
func new_sprite_velocity(direction) -> void:
	match direction:
		MOVE_DIRECTION.LEFT:
			sprite_vel = Vector2(rand_range(-1, -0.5), rand_range(-0.2,0.2)).normalized()
			move_sprite_direction = direction
		MOVE_DIRECTION.RIGHT:
			sprite_vel = Vector2(rand_range(0.5,1), rand_range(-0.2,0.2)).normalized()
			move_sprite_direction = direction
	
	if sprite_vel.x < 0:
		$AnimatedSprite.flip_h = true
		
	elif sprite_vel.x > 0 :
		$AnimatedSprite.flip_h = false
	
	#We've changed velocity. Let's see if we need to change rotation to match
	if sprite_vel.y < 0:
		var rotate_neko = rad2deg(tan(vel.y/vel.x))
		rotate_neko = clamp(rotate_neko, -5, 5)
		$AnimatedSprite.rotation_degrees = rotate_neko
		#print("rotate rad: ", str(tan(vel.y/vel.x)), " rotate deg: ", str(rotate_neko))
		
	else:
		var rotate_neko = rad2deg(tan(vel.y/vel.x))
		rotate_neko = clamp(rotate_neko, -5, 5)
		$AnimatedSprite.rotation_degrees = rotate_neko
		#print("rotate rad: ", str(tan(vel.y/vel.x)), " rotate deg: ", str(rotate_neko))	


func off_screen_period() -> void:
	#we trigger the delay timer, and stop the noise
	$OffscreenDelayTimer.start()
	$MovementSounds.stop()
	#$CatVocalisations.stop() 
	$ActionTimer.stop()

func play_next_animation() -> void:
	if animation_queue.size() > 0:
		$AnimatedSprite.animation = animation_queue[0]
		#print("Playing: ", animation_queue[0])
		if animation_queue[0] == "walking" or animation_queue[0] == "sliding":
			#let's slow down the speed if walking or sliding
			speed_multi = speed_slow
			$MovementSounds.stream = load("res://assets/sounds/cat_walking.wav")
			$MovementSounds.play()
		else:
			#otherwise, go full speed
			speed_multi = speed_max
			$MovementSounds.stream = load("res://assets/sounds/cat_running.wav")
			$MovementSounds.play()
			
		$AnimatedSprite.play()
		#print("animation queue: ", str(animation_queue))
		animation_queue.remove(0) #We're playing it, now remove it.	

func play_next_vocalisation() -> void:
	if sound_queue.size() > 0:
		$CatVocalisations.stream = load(sound_queue[0])
		$CatVocalisations.play()
		#print("play queue: ", str(sound_queue))
		sound_queue.remove(0) #We're playing it, now remove it.


func print_diagnostics() -> void:
	print("****************** print_diagnostics start ****************")
	print(str(OS.get_time()))
	print("OS.window_position ", str(OS.window_position))
	print("$AnimatedSprite.rotation_degrees",str($AnimatedSprite.rotation_degrees)) 
	print("$AnimatedSprite.position ",str($AnimatedSprite.position))
	print("vel ", str(vel))
	print("game_state ", str(game_state))
	print("move_direction ", str(move_direction))
	print("animation_queue", animation_queue)
	print("sound_queue", sound_queue)
	print("Master dB", str(AudioServer.get_bus_volume_db(AudioServer.get_bus_index("Master"))))
	print("$AnimationPlayer.is_playing(): ", str($AnimationPlayer.is_playing())) 
	print("$AnimationPlayer.current_animation: ", $AnimationPlayer.current_animation)
	print("real_screen_left_edge: ", str(real_screen_left_edge))
	print("real_screen_right_edge: ", str(real_screen_right_edge))
	print("check_limbo_pos: ", str(check_limbo_pos))
	print("****************** print_diagnostics end ****************")

func quit():
	cat_vocalisation()
	get_tree().quit()

func rainbow_cat() -> void:
	print("Current animation: ", $AnimationPlayer.current_animation)
	if is_cat_rainbow:
		$AnimationPlayer.stop(true) 
		$AnimationPlayer.seek(0, true) #Reset the frame back to start
		is_cat_rainbow = false
	else:
		#$AnimationPlayer.stop(true)
		$AnimationPlayer.play("Rainbow")
		is_cat_rainbow = true
		
	#print("rainbow_cat() animation: ", $AnimationPlayer.current_animation)
	
	
func unpause_game() -> void:
	#We are here because the user is not hovering over the cat or the menu
	#print_diagnostics()
	$PopupMenu.hide()
	if is_cat_rainbow:
		$AnimationPlayer.play("Rainbow")
		print("unpause_game(): Now Rainbow")
	else:
		$AnimationPlayer.stop(true)
		print("unpause_game(): stop animation")
		
	if !$AboutDialog.visible:
		#we can only unpause if the About dialog is off
		#reset game to previous movement
		game_state = game_state_previous
		#print_diagnostics()
		play_next_animation()
		$MovementSounds.play()
		$AnimatedSprite.play()
		
		
	
func _on_ActionTimer_timeout():
	#print_diagnostics()
	play_next_vocalisation()
	#just in case, let's also unpause the game if still paused
	if popup_menu_active and mouse_hover_over_cat:
		unpause_game()
		
	check_limbo_status()

func _on_Area2D_mouse_entered():
	if game_state == GAME_STATE.MOVE_NORMAL:
		$AnimatedSprite.stop()
		$MovementSounds.stop()
		#get_tree().paused = true
		game_state = GAME_STATE.PAUSED
		cat_purring()
		$AnimationPlayer.play("Idle")
		print("_on_Area2D_mouse_entered(): Now Idle")

func _on_Area2D_mouse_exited():
	mouse_hover_over_cat = false
	if not popup_menu_active and not mouse_hover_over_cat:
		unpause_game()

func _on_Area2D_input_event(viewport, event, shape_idx):
	if (event is InputEventMouseButton && event.pressed && event.button_index == BUTTON_RIGHT):
		#we are here bacuase the user right-button clicked on cat
		var menu_pos = get_global_mouse_position()
		menu_pos.x = clamp(menu_pos.x, 50, 100)
		menu_pos.y = clamp(menu_pos.y, 20, 50)
		$PopupMenu.set_position(menu_pos)
		$PopupMenu.show()
		popup_menu_active = true
	elif (event is InputEventMouseButton && event.pressed && event.button_index == BUTTON_LEFT):
		cat_meowing()



func _on_PopupMenu_id_pressed(id):
	var menu_item_selected = $PopupMenu.get_item_text(id)
	match menu_item_selected:
		"Pat Neko" :
			pat_neko()
		"Feed Neko" :
			feed_neko()
		"Increase Volume" : 
			increase_volume()
		"Decrease Volume" :
			decrease_volume()
		"Increase Appearances" : 
			increase_appearances()
		"Decrease Appearances" :
			decrease_appearances()
		"Rainbow Cat" : 
			rainbow_cat()
		"About Neko" : 
			about_neko()
		"Quit" :
			quit()
	popup_menu_active = false
	unpause_game()

func _on_OffscreenDelayTimer_timeout():
	#We are here because the cat has had enough time off-screen, and will now reverse direction
	#print_diagnostics()
	#reset the wait timer duration until next appearance
	$OffscreenDelayTimer.wait_time = rand_range(appearance_delay_min, appearance_delay_max)
	neko_paused = false
	$ActionTimer.start()
	$MovementSounds.play()
	cat_vocalisation()
	if move_direction == MOVE_DIRECTION.RIGHT:
		game_state = GAME_STATE.MOVE_SPRITE_RIGHT_REVERSE_LEFT
		new_sprite_velocity(MOVE_DIRECTION.LEFT)
	elif move_direction == MOVE_DIRECTION.LEFT:
		#We're still travelling to the right, which means that we now have to switch to MOVE_SPRITE_LEFT_REVERSE_RIGHT
		game_state = GAME_STATE.MOVE_SPRITE_LEFT_REVERSE_RIGHT
		new_sprite_velocity(MOVE_DIRECTION.RIGHT)
	
	
	print(str(move_direction))

func _on_CatVocalisations_finished():
	#print("_on_CatVocalisations_finished()")
	#current sound finished. Play next in queue, if there is a track
	if sound_queue.size() > 0 and game_state == GAME_STATE.MOVE_NORMAL:
		play_next_vocalisation()


func _on_AnimatedSprite_animation_finished():
	#print("_on_AnimatedSprite_animation_finished()")
	if animation_queue.size() > 0 and game_state == GAME_STATE.MOVE_NORMAL:
		#We only change the animation if this is a normal move.
		play_next_animation()
		

func _on_PopupMenu_mouse_exited():
	popup_menu_active = false
	unpause_game()
	


func _on_AboutDialog_confirmed():
	unpause_game()


func _on_AnimationPlayer_animation_changed(old_name, new_name):
	print("Anim changed")


func _on_AnimationPlayer_animation_started(anim_name):
	pass


func _on_AnimationPlayer_animation_finished(anim_name):
	pass # Replace with function body.
